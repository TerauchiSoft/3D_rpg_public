﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BattleCamera : MonoBehaviour
{
    /*~----------------------------------/
    :                                    :
    :             外部クラス             :
    :                                    :
    /~----------------------------------*/
    BattleControl bc;
    /*~----------------------------------/
    :                                    :
    :       カメラ及びカメラ描画範囲     :
    :                                    :
    /~----------------------------------*/
    public Camera camera_EnemyObjectCamera;     // カメラ及び撮影物格納オブジェクト
    public Camera camera_PlayerObjectCamera;    // カメラ及び撮影物格納オブジェクト

    public Camera camera_EnemyBackGroundCamera;     // カメラ及び撮影物格納オブジェクト
    public Camera camera_PlayerBackGroundCamera;    // カメラ及び撮影物格納オブジェクト

    public GameObject ob_EffectPlayObjectEnemy;     // エフェクトを再生するオブジェクト
    public GameObject ob_EffectPlayObjectPlayer;     // エフェクトを再生するオブジェクト

    private void OnEnable()
    {
        bc = GetComponent<BattleControl>();

        CameraInit();
        EnemyCameraOn();
    }

    // バトルの場面ごとのカメラ位置の名称
    public enum View
    {
        normal, select, appear1
    }

    public readonly Vector3[] EnmCamVecs = new Vector3[10]; // 場面ごとの敵のカメラの座標(主にz軸が重要)
    public readonly Vector3[] PlyCamVecs = new Vector3[10]; // 場面ごとのプレイヤ－のカメラの座標(主にz軸が重要)

    public readonly Vector3[] EnmBackCamVecs = new Vector3[10]; // 場面ごとの敵の背景のカメラの座標(主にz軸が重要)
    public readonly Vector3[] PlyBackCamVecs = new Vector3[10]; // 場面ごとのプレイヤ－の背景のカメラの座標(主にz軸が重要)

    public GameObject ob_EnemyObject;   // 敵、プレイヤーごとのカメラ描画範囲。ここの子にオブジェクトを入れて再生する。
    public GameObject ob_PlayerObject;

    /// <summary>敵のカメラの状態</summary>
    public bool Camera_EnemyEnabled
    {
        get
        { return camera_EnemyObjectCamera.enabled; }
        set
        {
            camera_EnemyObjectCamera.enabled = value;
            camera_EnemyBackGroundCamera.enabled = value;
        }
    }

    /// <summary>プレイヤーのカメラの状態</summary>
    public bool Camera_PlayerEnabled
    {
        get
        { return camera_PlayerObjectCamera.enabled; }
        set
        {
            camera_PlayerObjectCamera.enabled = value;
            camera_PlayerBackGroundCamera.enabled = value;
        }
    }

    /*~----------------------------------/
    :                                    :
    :         カメラ関係の関数           :
    :                                    :
    /~----------------------------------*/
    // カメラの初期動作設定
    void CameraInit()
    {
        // オブジェクトのカメラの設定

        PlyCamVecs[(int)View.normal] = new Vector3(300, 0, -20);
        EnmCamVecs[(int)View.normal] = new Vector3(200, 0, -20);

        PlyCamVecs[(int)View.appear1] = new Vector3(300, 0, -14);
        EnmCamVecs[(int)View.appear1] = new Vector3(200, 0, -14);

        PlyCamVecs[(int)View.select] = new Vector3(300, 4.7f, -10.47f);
        EnmCamVecs[(int)View.select] = new Vector3(200, 4.7f, -10.47f);


        // 背景のカメラの設定

        PlyBackCamVecs[(int)View.normal] = new Vector3(80f, 0, -10f);
        EnmBackCamVecs[(int)View.normal] = new Vector3(40f, 0, -10f);

        PlyBackCamVecs[(int)View.appear1] = new Vector3(80f, 0, -7f);
        EnmBackCamVecs[(int)View.appear1] = new Vector3(40f, 0, -7f);

        PlyBackCamVecs[(int)View.select] = new Vector3(80f, 0, -7.5f);
        EnmBackCamVecs[(int)View.select] = new Vector3(40f, 0, -7.5f);

        CameraZoom(camera_EnemyObjectCamera, EnmCamVecs[(int)View.appear1], EnmCamVecs[(int)View.normal], 1.0f);
        CameraZoom(camera_EnemyBackGroundCamera, EnmBackCamVecs[(int)View.appear1], EnmBackCamVecs[(int)View.normal], 1.0f);

    }

    /// <summary>指定位置から指定位置までDGTweenで移動する。</summary>
    public void CameraZoom(Camera cam, Vector3 beforevec, Vector3 aftervec, float time)
    {
        cam.transform.position = beforevec;
        cam.transform.DOMove(aftervec, time);
    }

    /// <summary>現在位置から指定位置までDGTweenで移動する。コルーチンでのウェイト付き。</summary>
    public IEnumerator CameraZoom(Camera cam, Vector3 aftervec, float time, float delay)
    {
        yield return new WaitForSeconds(delay);
        cam.transform.DOMove(aftervec, time);
    }

    /// <summary>指定位置から指定位置までDGTweenで移動する。コルーチンでのウェイト付き。</summary>
    public IEnumerator CameraZoom(Camera cam, Vector3 beforevec, Vector3 aftervec, float time, float delay)
    {
        yield return new WaitForSeconds(delay);
        cam.transform.position = beforevec;
        cam.transform.DOMove(aftervec, time);
    }

    /// <summary>キャラクターの位置までDGTweenで移動する。</summary>
    public int CameraMoveToCharacter(Battle_Status.Target myself, Battle_Status.Target tgt, int idx)
    {
        print("対象カメラ");
        Vector3 UsrVec;
        Vector3 BackVec;
        Camera[] cams = new Camera[2];
        if (tgt == Battle_Status.Target.Myself)
        {
            print("Myself");
            UsrVec = PlyCamVecs[(int)View.normal];
            UsrVec.x = (myself == Battle_Status.Target.Player) ? bc.PlayerPrefab[idx].transform.position.x : bc.EnemyPrefab[idx].transform.position.x;
            BackVec = (myself == Battle_Status.Target.Player) ? PlyBackCamVecs[(int)View.normal] : EnmBackCamVecs[(int)View.normal];
            cams[0] = (myself == Battle_Status.Target.Player) ? camera_PlayerObjectCamera : camera_EnemyObjectCamera;
            cams[1] = (myself == Battle_Status.Target.Player) ? camera_PlayerBackGroundCamera : camera_EnemyBackGroundCamera;
            tgt = myself;
        }
        else if (tgt == Battle_Status.Target.Player || tgt == Battle_Status.Target.Players)
        {
            print("プレイヤー");
            UsrVec = PlyCamVecs[(int)View.normal];
            UsrVec.x = bc.PlayerPrefab[idx].transform.position.x;
            BackVec = PlyBackCamVecs[(int)View.normal];
            cams[0] = camera_PlayerObjectCamera;
            cams[1] = camera_PlayerBackGroundCamera;
        }
        else //if (tgt == Battle_Status.Target.Enemy || tgt == Battle_Status.Target.Enemys)
        {
            print("えねみー");
            UsrVec = EnmCamVecs[(int)View.normal];
            UsrVec.x = bc.EnemyPrefab[idx].transform.position.x;
            BackVec = EnmBackCamVecs[(int)View.normal];
            cams[0] = camera_EnemyObjectCamera;
            cams[1] = camera_EnemyBackGroundCamera;
        }

        if (((tgt == Battle_Status.Target.Player || tgt == Battle_Status.Target.Players) && Camera_PlayerEnabled == false) ||
            ((tgt == Battle_Status.Target.Enemy || tgt == Battle_Status.Target.Enemys) && Camera_EnemyEnabled == false))
        {
            CameraChange();
            StartCoroutine(CameraZoom(cams[0], UsrVec, 0.3f, 0.7f));
            StartCoroutine(CameraZoom(cams[1], BackVec, 0.3f, 0.7f));
            return 1;
        }
        else
        {
            StartCoroutine(CameraZoom(cams[0], UsrVec, 0.35f, 0.3f));
            StartCoroutine(CameraZoom(cams[1], BackVec, 0.35f, 0.3f));
            return 0;
        }
    }

    /// <summary>
    /// 有効なカメラを取得
    /// </summary>
    Camera[] EnableCameras
    {
        get
        {
            Camera[] cams = new Camera[2];
            if (Camera_PlayerEnabled == true)
            {
                cams[0] = camera_PlayerObjectCamera;
                cams[1] = camera_PlayerBackGroundCamera;
            }
            else
            {
                cams[0] = camera_EnemyObjectCamera;
                cams[1] = camera_EnemyBackGroundCamera;
            }

            return cams;
        }
    }

    /// <summary>敵側のカメラのみ有効</summary>
    void EnemyCameraOn()
    {
        Camera_EnemyEnabled = true;
        Camera_PlayerEnabled = false;
    }

    /// <summary>プレイヤー側のカメラのみ有効</summary>
    public void PlayerCameraOn()
    {
        Camera_EnemyEnabled = false;
        Camera_PlayerEnabled = true;
    }

    /// <summary>有効のカメラを入れ替える</summary>
    public void CameraChange()
    {
        StartCoroutine(CameraChangeEffect());
    }


    // カメラ切替時の演出
    IEnumerator CameraChangeEffect()
    {
        EffectPlayOnObject(ob_EffectPlayObjectEnemy, "CameraChange");
        EffectPlayOnObject(ob_EffectPlayObjectPlayer, "CameraChange");


        int tim = 30;
        while (tim > 0)
        {
            if (tim == 15)
            {
                Camera_EnemyEnabled ^= true;
                Camera_PlayerEnabled ^= true;

                // カメラ位置初期化
                CameraPositionReset();
            }

            tim--;
            yield return 0;
        }

        yield return 0;
    }

    /// <summary> カメラ位置リセット </summary>
    void CameraPositionReset()
    {
        camera_EnemyObjectCamera.transform.position = EnmCamVecs[(int)View.normal];
        camera_PlayerObjectCamera.transform.position = PlyCamVecs[(int)View.normal];
        camera_EnemyBackGroundCamera.transform.position = EnmBackCamVecs[(int)View.normal];
        camera_PlayerBackGroundCamera.transform.position = PlyBackCamVecs[(int)View.normal];
    }

    // オブジェクト上の座標でエフェクト再生(Scaleも影響を受ける)
    public void EffectPlayOnObject(GameObject ob, string st)
    {
        EffekseerHandle handle = EffekseerSystem.PlayEffect(st, ob.transform.position);
        if (handle.enabled != false) print(ob.transform.position);
    }
    public void EffectPlayOnObject(Transform tr, string st)
    {
        /*EffekseerHandle handle = */
        EffekseerHandle handle = EffekseerSystem.PlayEffect(st, tr.position);
        if (handle.enabled != false) print(tr.position);
    }

    /// <summary>
    /// セレクト画面に切り替え
    /// </summary>
    /// <param name="xvec"></param>
    public void CameraMoveToSelect(Vector3 xvec)
    {
        StartCoroutine(CameraZoom(camera_PlayerObjectCamera, camera_PlayerObjectCamera.transform.position, PlyCamVecs[(int)View.select] + xvec, 0.8f, 0.4f));
        StartCoroutine(CameraZoom(camera_PlayerBackGroundCamera, camera_PlayerBackGroundCamera.transform.position, PlyBackCamVecs[(int)View.select], 0.8f, 0.4f));
    }

    /// <summary>
    /// 全体が見渡せる画面に切り替え
    /// </summary>
    public void CameraMoveToNormal()
    {
        CameraZoom(camera_PlayerObjectCamera, camera_PlayerObjectCamera.transform.position, PlyCamVecs[(int)View.normal], 1.0f);
        CameraZoom(camera_PlayerBackGroundCamera, camera_PlayerBackGroundCamera.transform.position, PlyBackCamVecs[(int)View.normal], 1.0f);
    }

}
