using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class SkillBook : Book
{
    public List<Skill> PlayerSkills;
    public GameObject[] ob_SkillText;
    public Animator[] anim_SkillText;
    public Animator anim_SkillBack;
    public GameObject ob_SkillBack;
    public TextMeshProUGUI[] text_SkillText;
    private int ButtonNum = 0;

    private bool isCanSelect = false;

    private int CursorLocation = 0;
    const int CursorWait = 10;
    int CursorWaitCount = 30;

    // Use this for initialization
    void Start()
    {
        plycnt = GameObject.Find("cdObject").GetComponent<PlayerController>();
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        if (bmb != null)
        {   // バトル
            if (bmb.isActSet == false || isCanSelect == false || bmb.selmenu != Battle_MenuButton.SelectMenu.Skill) return;
        }
        else
        {   // マップ
            if (isCanSelect == false) return;
        }

        var x = plycnt.Horizontal;
        var y = plycnt.Virtical;
        var enter = plycnt.isFire1;

        //print("入力" + x + "::" + y + " : " + CursorLocation);

        CursorMove(x, y);
        CursorButtonEvent(enter);
    }

    void CursorMove(float x, float y)
    {
        // 連続で流れるのを防止
        if (CursorWaitCount > 0)
        {
            CursorWaitCount--;
            return;
        }


        if (x > 0 || y < 0)
        {
            CursorLocation++;
            SelectAudio(); 
        }
        else if (x < 0 || y > 0)
        {
            CursorLocation--;
            SelectAudio(); 
        }
        else
            return;

        if (CursorLocation < 0) CursorLocation = ButtonNum;
        else if (CursorLocation > ButtonNum) CursorLocation = 0;

        SelectWaitAndAnimation();
    }

    void SelectAudio()
    {
        bmb.audioSource.clip = bmb.audio_select;
        bmb.audioSource.Play();
    }

    void SelectWaitAndAnimation()
    {
        ExplanatoryTextChange();
        CursorWaitCount = CursorWait;
        InitSelectAnime();
        if (CursorLocation != ButtonNum)
            anim_SkillText[CursorLocation].Play("Select", 0);
        else
            anim_SkillBack.Play("Select", 0);
    }

    public TextMeshProUGUI text_Attribute;
    public TextMeshProUGUI text_Range;
    public TextMeshProUGUI text_Exptext;
    void ExplanatoryTextChange()
    {
        if (CursorLocation == ButtonNum) return;

        // Player, Players, Enemy, Enemys, Myself
        string[] r_str = { "単", "全", "単", "全", "自" };
        // Normal, Fire, Plant, Water, Light, Dark
        string[] a_str = { "無", "火", "自", "水", "光", "暗" };

        int tgt = (int)PlayerSkills[CursorLocation].Target;
        text_Range.text = r_str[tgt];

        int atr = (int)PlayerSkills[CursorLocation].attribute;
        text_Attribute.text = a_str[atr];

        text_Exptext.text = PlayerSkills[CursorLocation].ExplanatoryText;
    }

    void InitSelectAnime()
    {
        foreach (var a in anim_SkillText)
            a.Play("Idle", 0);
        anim_SkillBack.Play("Idle", 0);
    }

    void CursorButtonEvent(bool enter)
    {
        if (enter == false) return;
        // 連続で押されるのを防止
        if (CursorWaitCount > 0) return;

        CursorWaitCount = CursorWait;

        bmb.audioSource.clip = bmb.audio_decision;
        bmb.audioSource.Play();
        ClickEvent(CursorLocation);

    }


    /// <summary>
    /// プレイヤースキルセット
    /// </summary>
    /// <param name="ob"></param>
    public void SetSkill(GameObject ob)
    {
        var bs = ob.GetComponent<Battle_Status>();
        this.bs = bs;
        PlayerSkills = ob.GetComponent<Battle_PlayerAct>().skills;

        ButtonEventSet(true);
        var backbtn = ob_SkillBack.GetComponent<Button>();
        backbtn.onClick.AddListener(() => ClickEvent(ButtonNum));
    }

    /// <summary>
    /// マップ用のスキルセット
    /// </summary>
    /// <param name="ob"></param>
    public void SetSkill(PlayerData pd)
    {
        // bs無しで良い
        PlayerSkills.Clear();
        PlayerSkills.AddRange(pd.Ply_skl);

        ButtonEventSet(false);
        var backbtn = ob_SkillBack.GetComponent<Button>();
        backbtn.onClick.AddListener(() => ClickEventOnTheMap(ButtonNum));
    }

    void ButtonEventSet(bool isBattle)
    {
        ButtonNum = 0;
        for (int i = 0; i < PlayerSkills.Count; i++)
        {
            // TODO:ページに対応
            if (i == 9) return;

            int ii = 0;
            ii += i;
            Skill skl = PlayerSkills[i];

            print("i::" + i);
            print("ii::" + ii);

            text_SkillText[ii] = ob_SkillText[ii].GetComponent<TextMeshProUGUI>();
            text_SkillText[ii].text = PlayerSkills[ii].SkillName;

            var btn = ob_SkillText[ii].GetComponent<Button>();
            ButtonNum++;
            if (isBattle == true)
                btn.onClick.AddListener(() => ClickEvent(ii));
            else
                btn.onClick.AddListener(() => ClickEventOnTheMap(ii));
        }
    }

    void ClickEvent(int n)
    {
        ClickEventCommon(n);
        if (n == ButtonNum) return;

        Skill skl = PlayerSkills[n];
        WhenOnClickBattleSkillSet(n, skl);
    }

    void ClickEventOnTheMap(int n)
    {
        ClickEventCommon(n);
        if (n == ButtonNum) return;

        Skill skl = PlayerSkills[n];
        WhenOnClickMapSkillSet(n, skl);
    }

    void ClickEventCommon(int n)
    {
        isCanSelect = false;
        HideBook();

        if (n == ButtonNum)
        {
            CursorButtonEvent(true);
            bmb.isCanSelect = true;
            bmb.isActSet = false;
            StartCoroutine(ToSelectOn());
            return;
        }
    }

    void WhenOnClickBattleSkillSet(int i, Skill skl)
    {
        bmb.BattleUIHide();

        bmb.btlctl.Players[bmb.ObjectID].TargetObject = (Battle_Status.Target)skl.Target;
        // TODO 敵の選択をできるようにする。
        bs.SelectedTgtNum = 0;
        bmb.btlctl.Players[bmb.ObjectID].NextAct = Battle_Status.Act.Skill;

        bmb.btlctl.Players[bmb.ObjectID].SelectedSkl = skl;
        bs.MpConsumption();
        bmb.BattleUIHide();
        bmb.btlctl.Phese = BattleControl.ST.BATTLE;
        print("Skill");
    }

    void WhenOnClickMapSkillSet(int i, Skill skl)
    {
        return;
        // TODO::mapact

        bmb.btlctl.Players[bmb.ObjectID].TargetObject = (Battle_Status.Target)skl.Target;
        // TODO 敵の選択をできるようにする。
        bs.SelectedTgtNum = 0;
        bmb.btlctl.Players[bmb.ObjectID].NextAct = Battle_Status.Act.Skill;

        bmb.btlctl.Players[bmb.ObjectID].SelectedSkl = skl;
        bs.MpConsumption();
        bmb.BattleUIHide();
        bmb.btlctl.Phese = BattleControl.ST.BATTLE;
        print("Skill");
    }
    
    /// <summary>
    /// ブックの表示
    /// </summary>
    public void ShowBook()
    {
        plycnt.SetKeyDownMode(true);
        StartCoroutine(ToSelectOn());
        anim.Play("Show", 0);
        SelectWaitAndAnimation();
    }

    /// <summary>
    /// スキルブックの非表示
    /// </summary>
    public void HideBook()
    {
        base.CallBack();
        CursorWaitCount = CursorWait;
        InitSelectAnime();
        anim.Play("Hide", 0);
    }

    IEnumerator ToSelectOn()
    {
        yield return new WaitForSecondsRealtime(0.4f);
        isCanSelect = true;
        SelectWaitAndAnimation();
    }
}
