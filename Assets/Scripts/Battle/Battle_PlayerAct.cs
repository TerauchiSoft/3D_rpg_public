﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 敵のスキルとスキルの思考ルーチン
/// </summary>
public class Battle_PlayerAct : MonoBehaviour
{
    /*~----------------------------------/
    :                                    :
    :           外部のクラス             :
    :                                    :
    /~----------------------------------*/
    // テーブル
    SkillTable skl_table;
    ItemTable itm_table;

    /*~----------------------------------/
    :                                    :
    :              クラス                :
    :                                    :
    /~----------------------------------*/
    // 乱数
    static System.Random rand = new System.Random();
    /*~----------------------------------/
    :                                    :
    :         外部から設定する変数       :
    :                                    :
    /~----------------------------------*/
    /// <summary>
    /// キャラの名前
    /// </summary>
    public string Name;

    /// <summary>
    /// 何番目のキャラ(1 ~ 3)
    /// </summary>
    public int Number;

    /// <summary>
    /// スキルデータを読み込むための名前(SkillTable.SkillID)
    /// </summary>
    public SkillTable.SkillID[] SkillNames;
    /// <summary>
    /// アイテムデータを読み込むための名前
    /// </summary>
    public ItemTable.ItemID[] ItemNames;

    /*~----------------------------------------------------------/
    :                                                            :
    :       コモンのテーブルから読み込んで設定設定する変数       :
    :                                                            :
    /~----------------------------------------------------------*/
    /// <summary>
    /// プレイヤーのスキル、最大で10~
    /// SkillNamesでcdObjectのテーブルから読み込む
    /// </summary>
    public List<Skill> skills;
    public List<Item> items;

    private void Start()
    {
        skl_table = GameObject.Find("cdObject").GetComponent<SkillTable>();
        itm_table = GameObject.Find("cdObject").GetComponent<ItemTable>();

        SkillLoadFromCommonData();
        /*
        SkillLoadFromUnityList()
        */
    }

    // UnityのInspectorに入力した情報から
    void SkillLoadFromUnityList()
    {
        for (int i = 0; i < SkillNames.Length; i++)
        {
            skills.Add(skl_table.GetSkillFromEffectName(SkillNames[i]));
        }
        for (int i = 0; i < ItemNames.Length; i++)
        {
            items.Add(itm_table.GetItemFromName(ItemNames[i]));
        }
    }

    void SkillLoadFromCommonData()
    {
        var cd = GameObject.Find("cdObject").GetComponent<CommonData>();
        for (int i = 0; i < cd.PlayerData[Number - 1].Ply_skl.Length; i++)
        {
            var skl = cd.PlayerData[Number - 1].Ply_skl[i];
            if (skl.skl_method != null) skills.Add(skl);
            else break;
        }
        for (int i = 0; i < cd.PlayerData[Number - 1].Ply_item.Length; i++)
        {
            var itm = cd.PlayerData[Number - 1].Ply_item[i];
            if (itm.itm_method != null) items.Add(itm);
            else break;
        }
    }
}
