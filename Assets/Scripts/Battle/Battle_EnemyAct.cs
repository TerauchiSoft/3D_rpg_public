﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 敵のスキルとスキルの思考ルーチン
/// </summary>
public class Battle_EnemyAct : MonoBehaviour
{
    /*~----------------------------------/
    :                                    :
    :           外部のクラス             :
    :                                    :
    /~----------------------------------*/
    // テーブル
    SkillTable skl_table;

    /*~----------------------------------/
    :                                    :
    :              クラス                :
    :                                    :
    /~----------------------------------*/
    // 乱数
    static System.Random rand = new System.Random();
    /*~----------------------------------/
    :                                    :
    :         外部から設定する変数       :
    :                                    :
    /~----------------------------------*/
    /// <summary>
    /// キャラの名前
    /// </summary>
    public ActaiID Name;

    /// <summary>
    /// スキルデータを読み込むための名前(SkillTable.SkillID)
    /// </summary>
    public SkillTable.SkillID[] SkillNames;
    
    /*~----------------------------------------------------------/
    :                                                            :
    :       コモンのテーブルから読み込んで設定設定する変数       :
    :                                                            :
    /~----------------------------------------------------------*/
    /// <summary>
    /// 敵のスキル、最大で4~5
    /// SkillNamesでcdObjectのテーブルから読み込む
    /// </summary>
    public Skill[] skills;

    /// <summary>
    /// スキルAIの関数、行動ルーチン
    /// </summary>
    public actai skl_ai_method;

    private void Start()
    {
        skills = new Skill[SkillNames.Length];

        skl_ai_method = GetSkillAiMethod(Name);
        skl_table = GameObject.Find("cdObject").GetComponent<SkillTable>();
        for (int i = 0; i < SkillNames.Length; i++)
        {
            skills[i] = skl_table.GetSkillFromEffectName(SkillNames[i]);
        }
    }

    /// <summary>
    /// 敵キャラの行動関数
    /// </summary>
    public delegate Battle_Status actai(Battle_Status b_st, List<Battle_Status> b_st_us, List<Battle_Status> b_st_tgt, Battle_EnemyAct skl);

    /// <summary>
    /// 敵キャラのID
    /// </summary>
    public enum ActaiID
    {
        Wolf = 0, FlowerSlime, Yashigani, Harpy, Alraune, Mashmellz, Dragonbaby, Dragonkids, DogGateKeeper,
        Automata, Dragon, Yale, DarkElf, Sphinx, StoneGolem, BlossomSlime, IronGolem, Dullahan, Mimic1, Mimic2, Mimic3,
        Fary, Frat 
    }

    /// <summary>
    /// 敵キャラのai
    /// </summary>
    /// <returns></returns>
    public static actai GetSkillAiMethod(ActaiID ID)
    {
        // デリゲートメソッド
        actai[] actais = {
            Wolf, FlowerSlime, Yashigani, Harpy, Alraune, Mashmellz, Dragonbaby, Dragonkids, DogGateKeeper,
            Automata, Dragon, Yale, DarkElf, Sphinx, StoneGolem, BlossomSlime, IronGolem, Dullahan, Mimic1, Mimic2, Mimic3,
            Fary, Frat
        };
        return actais[(int)ID];
    }

    /// <summary>
    /// 攻撃セット
    /// </summary>
    /// <param name="b_st"></param>
    /// <param name="idx"></param>
    /// <returns></returns>
    public static Battle_Status GetActAttack(Skill skl, Battle_Status b_st, List<Battle_Status> b_st_us, List<Battle_Status> b_st_tgt, int idx)
    {
        b_st.SelectedSkl = skl;
        b_st.NextAct = Battle_Status.Act.Skill;
        // TODO:全体化に対応
        if (skl.Target != Skill.Tgt.Myself && (int)skl.Target >= 2)
            skl.Target -= 2;
        else if ((int)skl.Target < 2)
            skl.Target += 2;
        b_st.TargetObject = (Battle_Status.Target)skl.Target;
        b_st.SelectedTgtNum = idx;
        return SetEnmStatus(b_st, b_st_us, b_st_tgt, idx);
    }

    /// <summary>
    /// 攻撃目標のステータスセット
    /// </summary>
    static Battle_Status SetEnmStatus(Battle_Status bs, List<Battle_Status> us_bs, List<Battle_Status> tgt_bs, int idx)
    {
        if (bs.TargetObject == Battle_Status.Target.Myself)
            bs.SetEnemyStatus(bs, us_bs, bs.ObjectID);
        else if (bs.TargetObject == Battle_Status.Target.Player)
            bs.SetEnemyStatus(bs, tgt_bs, idx);
        else if (bs.TargetObject == Battle_Status.Target.Players)
            bs.SetEnemyStatus(bs, tgt_bs);
        else if (bs.TargetObject == Battle_Status.Target.Enemy)
            bs.SetEnemyStatus(bs, us_bs, idx);
        else //if (bs.TargetObject == Battle_Status.Target.Enemys)
            bs.SetEnemyStatus(bs, us_bs);

        return bs;
    }

    /// <summary>
    /// おおかみおとこ
    /// </summary>
    /// <returns></returns>
    public static Battle_Status Wolf(Battle_Status b_st, List<Battle_Status> b_st_us, List<Battle_Status> b_st_tgt, Battle_EnemyAct skl)
    {
        print("Wolf");
        var n = rand.Next(b_st_tgt.Count);
        var w = rand.Next(skl.skills.Length);
        b_st = GetActAttack(skl.skills[w], b_st, b_st_us, b_st_tgt, n);

        return b_st;
    }

    /// <summary>
    /// フラワースライム
    /// </summary>
    /// <returns></returns>
    public static Battle_Status FlowerSlime(Battle_Status b_st, List<Battle_Status> b_st_us, List<Battle_Status> b_st_tgt, Battle_EnemyAct skl)
    {
        var n = rand.Next(b_st_tgt.Count);
        var w = rand.Next(skl.skills.Length);
        b_st = GetActAttack(skl.skills[w], b_st, b_st_us, b_st_tgt, n);

        return b_st;
    }

    /// <summary>
    /// やしがにん
    /// </summary>
    /// <returns></returns>
    public static Battle_Status Yashigani(Battle_Status b_st, List<Battle_Status> b_st_us, List<Battle_Status> b_st_tgt, Battle_EnemyAct skl)
    {
        var n = rand.Next(b_st_tgt.Count);
        var w = rand.Next(skl.skills.Length);
        b_st = GetActAttack(skl.skills[w], b_st, b_st_us, b_st_tgt, n);

        return b_st;
    }

    /// <summary>
    /// ハーピー
    /// </summary>
    /// <returns></returns>
    public static Battle_Status Harpy(Battle_Status b_st, List<Battle_Status> b_st_us, List<Battle_Status> b_st_tgt, Battle_EnemyAct skl)
    {
        var n = rand.Next(b_st_tgt.Count);
        var w = rand.Next(skl.skills.Length);
        b_st = GetActAttack(skl.skills[w], b_st, b_st_us, b_st_tgt, n);

        return b_st;
    }

    /// <summary>
    /// アルラウネ
    /// </summary>
    /// <returns></returns>
    public static Battle_Status Alraune(Battle_Status b_st, List<Battle_Status> b_st_us, List<Battle_Status> b_st_tgt, Battle_EnemyAct skl)
    {
        var n = rand.Next(b_st_tgt.Count);
        var w = rand.Next(skl.skills.Length);
        b_st = GetActAttack(skl.skills[w], b_st, b_st_us, b_st_tgt, n);

        return b_st;
    }

    /// <summary>
    /// マッシュメルツ
    /// </summary>
    /// <returns></returns>
    public static Battle_Status Mashmellz(Battle_Status b_st, List<Battle_Status> b_st_us, List<Battle_Status> b_st_tgt, Battle_EnemyAct skl)
    {
        var n = rand.Next(b_st_tgt.Count);
        var w = rand.Next(skl.skills.Length);
        b_st = GetActAttack(skl.skills[w], b_st, b_st_us, b_st_tgt, n);

        return b_st;
    }

    /// <summary>
    /// ドラゴンベイビー
    /// </summary>
    /// <returns></returns>
    public static Battle_Status Dragonbaby(Battle_Status b_st, List<Battle_Status> b_st_us, List<Battle_Status> b_st_tgt, Battle_EnemyAct skl)
    {
        var n = rand.Next(b_st_tgt.Count);
        var w = rand.Next(skl.skills.Length);
        b_st = GetActAttack(skl.skills[w], b_st, b_st_us, b_st_tgt, n);

        return b_st;
    }

    /// <summary>
    /// ドラゴンキッズ
    /// </summary>
    /// <returns></returns>
    public static Battle_Status Dragonkids(Battle_Status b_st, List<Battle_Status> b_st_us, List<Battle_Status> b_st_tgt, Battle_EnemyAct skl)
    {
        var n = rand.Next(b_st_tgt.Count);
        var w = rand.Next(skl.skills.Length);
        b_st = GetActAttack(skl.skills[w], b_st, b_st_us, b_st_tgt, n);

        return b_st;
    }

    /// <summary>
    /// 犬門番
    /// </summary>
    /// <returns></returns>
    public static Battle_Status DogGateKeeper(Battle_Status b_st, List<Battle_Status> b_st_us, List<Battle_Status> b_st_tgt, Battle_EnemyAct skl)
    {
        var n = rand.Next(b_st_tgt.Count);
        var w = rand.Next(skl.skills.Length);
        b_st = GetActAttack(skl.skills[w], b_st, b_st_us, b_st_tgt, n);

        return b_st;
    }

    /// <summary>
    /// オートマタ
    /// </summary>
    /// <returns></returns>
    public static Battle_Status Automata(Battle_Status b_st, List<Battle_Status> b_st_us, List<Battle_Status> b_st_tgt, Battle_EnemyAct skl)
    {
        var n = rand.Next(b_st_tgt.Count);
        var w = rand.Next(skl.skills.Length);
        b_st = GetActAttack(skl.skills[w], b_st, b_st_us, b_st_tgt, n);

        return b_st;
    }

    /// <summary>
    /// ドラゴン
    /// </summary>
    /// <returns></returns>
    public static Battle_Status Dragon(Battle_Status b_st, List<Battle_Status> b_st_us, List<Battle_Status> b_st_tgt, Battle_EnemyAct skl)
    {
        var n = rand.Next(b_st_tgt.Count);
        var w = rand.Next(skl.skills.Length);
        b_st = GetActAttack(skl.skills[w], b_st, b_st_us, b_st_tgt, n);

        return b_st;
    }

    /// <summary>
    /// エアレー
    /// </summary>
    /// <returns></returns>
    public static Battle_Status Yale(Battle_Status b_st, List<Battle_Status> b_st_us, List<Battle_Status> b_st_tgt, Battle_EnemyAct skl)
    {
        var n = rand.Next(b_st_tgt.Count);
        var w = rand.Next(skl.skills.Length);
        b_st = GetActAttack(skl.skills[w], b_st, b_st_us, b_st_tgt, n);

        return b_st;
    }

    /// <summary>
    /// ダークエルフ
    /// </summary>
    /// <returns></returns>
    public static Battle_Status DarkElf(Battle_Status b_st, List<Battle_Status> b_st_us, List<Battle_Status> b_st_tgt, Battle_EnemyAct skl)
    {
        var n = rand.Next(b_st_tgt.Count);
        var w = rand.Next(skl.skills.Length);
        b_st = GetActAttack(skl.skills[w], b_st, b_st_us, b_st_tgt, n);

        return b_st;
    }

    /// <summary>
    /// スフィンクス
    /// </summary>
    /// <returns></returns>
    public static Battle_Status Sphinx(Battle_Status b_st, List<Battle_Status> b_st_us, List<Battle_Status> b_st_tgt, Battle_EnemyAct skl)
    {
        var n = rand.Next(b_st_tgt.Count);
        var w = rand.Next(skl.skills.Length);
        b_st = GetActAttack(skl.skills[w], b_st, b_st_us, b_st_tgt, n);

        return b_st;
    }

    /// <summary>
    /// スフィンクス
    /// </summary>
    /// <returns></returns>
    public static Battle_Status StoneGolem(Battle_Status b_st, List<Battle_Status> b_st_us, List<Battle_Status> b_st_tgt, Battle_EnemyAct skl)
    {
        var n = rand.Next(b_st_tgt.Count);
        var w = rand.Next(skl.skills.Length);
        b_st = GetActAttack(skl.skills[w], b_st, b_st_us, b_st_tgt, n);

        return b_st;
    }

    /// <summary>
    /// ブロッサムスライム
    /// </summary>
    /// <returns></returns>
    public static Battle_Status BlossomSlime(Battle_Status b_st, List<Battle_Status> b_st_us, List<Battle_Status> b_st_tgt, Battle_EnemyAct skl)
    {
        var n = rand.Next(b_st_tgt.Count);
        var w = rand.Next(skl.skills.Length);
        b_st = GetActAttack(skl.skills[w], b_st, b_st_us, b_st_tgt, n);

        return b_st;
    }

    /// <summary>
    /// アイアンゴーレム
    /// </summary>
    /// <returns></returns>
    public static Battle_Status IronGolem(Battle_Status b_st, List<Battle_Status> b_st_us, List<Battle_Status> b_st_tgt, Battle_EnemyAct skl)
    {
        var n = rand.Next(b_st_tgt.Count);
        var w = rand.Next(skl.skills.Length);
        b_st = GetActAttack(skl.skills[w], b_st, b_st_us, b_st_tgt, n);

        return b_st;
    }

    /// <summary>
    /// デュラハン
    /// </summary>
    /// <returns></returns>
    public static Battle_Status Dullahan(Battle_Status b_st, List<Battle_Status> b_st_us, List<Battle_Status> b_st_tgt, Battle_EnemyAct skl)
    {
        var n = rand.Next(b_st_tgt.Count);
        var w = rand.Next(skl.skills.Length);
        b_st = GetActAttack(skl.skills[w], b_st, b_st_us, b_st_tgt, n);

        return b_st;
    }

    /// <summary>
    /// ボロボロミミック
    /// </summary>
    /// <returns></returns>
    public static Battle_Status Mimic1(Battle_Status b_st, List<Battle_Status> b_st_us, List<Battle_Status> b_st_tgt, Battle_EnemyAct skl)
    {
        var n = rand.Next(b_st_tgt.Count);
        var w = rand.Next(skl.skills.Length);
        b_st = GetActAttack(skl.skills[w], b_st, b_st_us, b_st_tgt, n);

        return b_st;
    }

    /// <summary>
    /// そこそこミミック
    /// </summary>
    /// <returns></returns>
    public static Battle_Status Mimic2(Battle_Status b_st, List<Battle_Status> b_st_us, List<Battle_Status> b_st_tgt, Battle_EnemyAct skl)
    {
        var n = rand.Next(b_st_tgt.Count);
        var w = rand.Next(skl.skills.Length);
        b_st = GetActAttack(skl.skills[w], b_st, b_st_us, b_st_tgt, n);

        return b_st;
    }

    /// <summary>
    /// PIKAPIKAミミック
    /// </summary>
    /// <returns></returns>
    public static Battle_Status Mimic3(Battle_Status b_st, List<Battle_Status> b_st_us, List<Battle_Status> b_st_tgt, Battle_EnemyAct skl)
    {
        var n = rand.Next(b_st_tgt.Count);
        var w = rand.Next(skl.skills.Length);
        b_st = GetActAttack(skl.skills[w], b_st, b_st_us, b_st_tgt, n);

        return b_st;
    }
    

    /// <summary>
    /// フェアリー
    /// </summary>
    /// <returns></returns>
    public static Battle_Status Fary(Battle_Status b_st, List<Battle_Status> b_st_us, List<Battle_Status> b_st_tgt, Battle_EnemyAct skl)
    {
        var n = rand.Next(b_st_tgt.Count);
        var w = rand.Next(skl.skills.Length);
        b_st = GetActAttack(skl.skills[w], b_st, b_st_us, b_st_tgt, n);

        return b_st;
    }

    /// <summary>
    /// フレイムラット
    /// </summary>
    /// <returns></returns>
    public static Battle_Status Frat(Battle_Status b_st, List<Battle_Status> b_st_us, List<Battle_Status> b_st_tgt, Battle_EnemyAct skl)
    {
        var n = rand.Next(b_st_tgt.Count);
        var w = rand.Next(skl.skills.Length);
        b_st = GetActAttack(skl.skills[w], b_st, b_st_us, b_st_tgt, n);

        return b_st;
    }
    
}
