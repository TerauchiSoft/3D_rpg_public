﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LV : MonoBehaviour
{
    public Battle_Status btlst;

    public enum Like
    { Aresta, Enemy}
    public Like like;
    public int ID = 0;

    public int initLV = 0;

    public Slider s_LV;
    public Slider s_HP;
    public Slider s_MP;

    BattleControl bc;
    CommonData cd;
    public Text textlv;
    public Text texthp;
    public Text textmp;


    // Use this for initialization
    void Start ()
    {
        bc = GameObject.Find("BattleControl").GetComponent<BattleControl>();
        cd = GameObject.Find("cdObject").GetComponent<CommonData>();

        s_LV.onValueChanged.AddListener(lvhenkou);
        s_HP.onValueChanged.AddListener(hphenkou);
        s_MP.onValueChanged.AddListener(mphenkou);
    }

    int count = 30;
    private void Update()
    {
        if (count > 0)
        {
            count--;
            return;
        }
        else if (count == 0)
        {
            SliderValueChange(btlst);
            return;
        }
        else
            return;
    }

    public void lvhenkou(float lv)
    {
        if (like == Like.Enemy)
        {
            s_LV.value = initLV;
            return;
        }

        int ilv = (int)lv;
        if (ilv != cd.PlayerData[ID].LV)
            cd.PlayerDataSet(ilv);
        bc.SetCommonStatusToBattlePlayer(bc.Players[ID], cd.PlayerData[ID]);
        bc.Players[ID].StatusInit(ID, Battle_Status.Target.Player);
        SliderValueChange(bc.Players[ID]);
    }

    public void SliderValueChange(Battle_Status bs)
    {
        s_HP.maxValue = bs.M_Hp;
        s_HP.minValue = 0;
        s_MP.maxValue = bs.M_Mp;
        s_MP.minValue = 0;
        textlv.text = "LV:" + bs.Lv.ToString();
        texthp.text = "hp:" + bs.Hp.ToString() + "/" + bs.M_Hp.ToString();
        textmp.text = "mp:" + bs.Mp.ToString() + "/" + bs.M_Mp.ToString();
    }

    public void HpSliderHenkou(float hp)
    {
        s_HP.value = (int)hp;
    }

    public void MpSliderHenkou(float mp)
    {
        s_MP.value = (int)mp;
    }

    public void hphenkou(float hp)
    {
        btlst.Hp = (int)hp;
    }

    public void mphenkou(float mp)
    {
        btlst.Mp = (int)mp;
    }
}