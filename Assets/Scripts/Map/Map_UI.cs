﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;
using TMPro;

public class Map_UI : MonoBehaviour
{
    /*~***********************************
    *                                    *
    *            外部クラス	             *
    *                                    *
    /~***********************************/
    CommonData cd;
    PlayerController plycnt;
    public Map_Sound sound;
    public GameObject ob_TextWin;
    public TextMeshProUGUI text_TextWin;
    public MapEventSystem iv_sys;
    public SkillBook sklbook;
    public ItemBook itembook;
    /*~***********************************
    *                                    *
    *    このオブジェクトのクラス        *
    *                                    *
    /~***********************************/
    Animator anim_Map_UI;
    Battle_TextWin MapTextWin;
    /*~***********************************
    *                                    *
    *              カーソル物            *
    *                                    *
    /~***********************************/
    // アレスタ - magic, item, skill, エリナ - magic, skill, ペテル - magic, skill
    // マップ
    private readonly string[] animStateName = { "Aresta_magic", "Aresta_item", "Aresta_skill", "Erina_magic", "Erina_skill", "Petil_magic", "Petil_skill"};
    public GameObject[] obs_Chara = new GameObject[6];
    public GameObject[] ob_btns = new GameObject[7];
    /*~***********************************
    *                                    *
    *                変数類              *
    *                                    *
    /~***********************************/
    readonly int WaitCountFlame = 410;
    int WaitCount;

    bool OpenTheMenu = false;
    bool isMenuActive = true;
    
    // Use this for initialization
    void Start () {
        cd = GameObject.Find("cdObject").GetComponent<CommonData>();
        plycnt = GameObject.Find("cdObject").GetComponent<PlayerController>();
        anim_Map_UI = GetComponent<Animator>();
        MapTextWin = new Battle_TextWin(ob_TextWin, 4);
        WaitCount = WaitCountFlame;
        ClickEventSet();
        // 自動的に実行される
        // anim_Map_UI.Play("Map_UI_OutOfCanvas", 0);
    }
    
    
    // Update is called once per frame
    void Update () {
        // メニュー禁止
        if (plycnt.isMenuAllow == false) return;

        // スキル、アイテムを開いている。
        if (isMenuActive == false) return;

        // メニューを開いていない
        if (OpenTheMenu == false)
        {
            CharaViewJudgeForMenuOpen();
            KeyJudgeForMenuOpen();

            if (plycnt.IsButtonAndAxisIsNoZero == true) WaitCount = WaitCountFlame;
            if (WaitCount >= 0) WaitCount--;
            if (CursorWaitCount > 0) CursorWaitCount--;
        }
        // OpenTheMenu == true
        else
        {
            MenuRoutine();
            if (CursorWaitCount > 0)
                CursorWaitCount--;

            if (ClickWaitCount > 0)
                ClickWaitCount--;
        }
    }
    
    public int MenuCursorLocation = 0;
    readonly int CursorWait = 10;
    int CursorWaitCount = 30;
    /// <summary>
    /// メニュー
    /// </summary>
    void MenuRoutine()
    {
        if (anim_Map_UI.GetCurrentAnimatorStateInfo(0).IsName("Map_UI_Appear") == true)
            return;
        else
            anim_Map_UI.Play(animStateName[MenuCursorLocation], 0);

        var x = plycnt.Horizontal;
        var y = plycnt.Virtical;
        var enter = plycnt.isFire1;
        var cancel = plycnt.isFire2;

        CursorMove(x, y);
        CursorButtonEvent(enter, cancel);
        
    }
    
    void CursorMove(float x, float y)
    {
        if (CursorWaitCount == CursorWait - 1) anim_Map_UI.Play(animStateName[MenuCursorLocation], 0);
        if (CursorWaitCount == 0)
        {
            if (x != 0 || y != 0)
            {
                CursorWaitCount = CursorWait;
                sound.CursorMoveSoundPlay();
            }

            if (x > 0)
                MenuCursorLocation++;
            else if (x < 0)
                MenuCursorLocation--;

            if (y > 0)
                MenuCursorLocation--;
            else if (y < 0)
                MenuCursorLocation++;

            CursorMoveForCursorLimit();
        }
    }

    /// <summary>
    /// カーソルの限界点による移動
    /// </summary>
    void CursorMoveForCursorLimit()
    {
        if (MenuCursorLocation > animStateName.Length - 1) MenuCursorLocation = 0;
        else if (MenuCursorLocation < 0) MenuCursorLocation = animStateName.Length - 1;

        // 誰もいない
        if (cd.Aresta.isJoined == false && cd.Erina.isJoined == false && cd.Petil.isJoined == false)
        {
            MenuCursorLocation = 0;
            return;
        }

        int[] ns = { 0, 3, 5 };

        // アレスタがいなくなることは無いが一応(この時必然的にアイテムが使えない)
        if (cd.Aresta.isJoined == false)
        {
            if (MenuCursorLocation == 0)
            {
                if (cd.Erina.isJoined == true) MenuCursorLocation = 3;
                else if (cd.Petil.isJoined == true) MenuCursorLocation = 5;
            }
            else if (MenuCursorLocation == 2)
            {
                if (cd.Petil.isJoined == true) MenuCursorLocation = 6;
                else if (cd.Erina.isJoined == true) MenuCursorLocation = 4;
            }
        }
        
        // エリナ不在
        if (cd.Erina.isJoined == false)
        {
            if (MenuCursorLocation == 3)
            {
                if (cd.Petil.isJoined == true) MenuCursorLocation = 5;
                else if (cd.Aresta.isJoined == true) MenuCursorLocation = 0;
            }
            else if(MenuCursorLocation == 4)
            {
                if (cd.Aresta.isJoined == true) MenuCursorLocation = 2;
                else if (cd.Petil.isJoined == true) MenuCursorLocation = 6;
            }
        }

        // ペテル不在
        if (cd.Petil.isJoined == false)
        {
            if (MenuCursorLocation == 5)
            {
                if (cd.Aresta.isJoined == true) MenuCursorLocation = 0;
                else if (cd.Erina.isJoined == true) MenuCursorLocation = 3;
            }
            else if (MenuCursorLocation == 6)
            {
                if (cd.Erina.isJoined == true) MenuCursorLocation = 4;
                else if (cd.Aresta.isJoined == true) MenuCursorLocation = 2;
            }
        }
        
    }


    /************************************************
     *
     *          ボタン、クリック時のイベント
     * 
     * *********************************************/
    /// <summary>
    /// 決定、キャンセル。
    /// </summary>
    /// <param name="ent"></param>
    /// <param name="cnc"></param>
    void CursorButtonEvent(bool ent, bool cnc)
    {
        if (CursorWaitCount > 0) return;
        if (cnc == true)
        {
            MenuClose();
            return;
        }
        if (ent == true)
        {
            ClickEvent(MenuCursorLocation);
        }
    }

    void ClickEventSet()
    {
        // 歩き禁止
        plycnt.isMoveAllow = false;

        for (int i = 0; i < ob_btns.Length; i++)
        {
            int ii = 0;
            ii += i;
            try {
                ob_btns[ii].GetComponent<Button>().onClick.AddListener(() => ClickEvent(ii));
            }
            catch
            {
                print(ii);
            }
         }
    }

    int ClickWait = 60;
    int ClickWaitCount = 0;
    /// <summary>
    /// クリック時、ボタン決定時のイベント
    /// </summary>
    /// <param name="n"></param>
    void ClickEvent(int n)
    {
        switch (n)
        {
            // Aresuta
            // magic
            case 0:
                PlayMagicMessage(0);
                break;
            // item
            case 1:
                // ItemButtonEventFromUI(0);
                break;
            // skill
            case 2:
                // SkillButtonEventFromUI(0);
                break;
            // Erina
            // magic
            case 3:
                PlayMagicMessage(1);
                break;
            // skill
            case 4:
                // SkillButtonEventFromUI(1);
                break;
            // Petil
            // magic
            case 5:
                PlayMagicMessage(2);
                break;
            // skill
            case 6:
                // SkillButtonEventFromUI(2);
                break;
        }

    }

    private void PlayMagicMessage(int n)
    {
        string str = BattleControl.GetMpStr(cd.PlayerData[n].MP, cd.PlayerData[n].M_MP, cd.PlayerData[n].PlayerName);
        sound.DecisionSoundPlay();
        TextAreaOpen();
        StartStatusMes(str);
    }

    private void PlayVitalMessage(int n)
    {
        string str = BattleControl.GetHpStr(cd.PlayerData[n].HP, cd.PlayerData[n].M_HP, cd.PlayerData[n].PlayerName, Battle_Status.Target.Player);
        sound.DecisionSoundPlay();
        TextAreaOpen();
        StartStatusMes(str);
    }

    /// <summary>
    /// ステータス用のメッセージ再生
    /// </summary>
    /// <param name="str"></param>
    private void StartStatusMes(string str)
    {
        MapTextWin.SetViewSpan(0);
        StartCoroutine(MapTextWin.TextPlay(str));
    }

    void TextAreaOpen()
    {
        ob_TextWin.SetActive(true);
    }

    void TextAreaClose()
    {
        ob_TextWin.SetActive(false);
    }

    /// <summary>
    /// カウント0によるキャラクター表示
    /// </summary>
    void CharaViewJudgeForMenuOpen()
    {
        if ((WaitCount == 0 && plycnt.IsButtonAndAxisIsNoZero == false))
        {
            CharacterView();
        }
    }

    /// <summary>
    /// キーによるメニューオープンチェック
    /// </summary>
    void KeyJudgeForMenuOpen()
    {
        if (CursorWaitCount == 0 && plycnt.IsButtonAndAxisIsNoZero == true)
        {
            if (plycnt.isFire2 == true)
                MenuOpen();
            else
                CharacterHide();

            WaitCount = WaitCountFlame;
        }
    }

    /// <summary>
    /// 仲間にいるかで表示を変更
    /// </summary>
    void CharaShowOrHideByJoined()
    {
        if (cd.Aresta.isJoined == true)
        {
            obs_Chara[0].SetActive(true);
            obs_Chara[1].SetActive(true);
        }
        else
        {
            obs_Chara[0].SetActive(false);
            obs_Chara[1].SetActive(false);
        }

        if (cd.Erina.isJoined == true)
        {
            obs_Chara[2].SetActive(true);
            obs_Chara[3].SetActive(true);
        }
        else
        {
            obs_Chara[2].SetActive(false);
            obs_Chara[3].SetActive(false);
        }

        if (cd.Petil.isJoined == true)
        {
            obs_Chara[4].SetActive(true);
            obs_Chara[5].SetActive(true);
        }
        else
        {
            obs_Chara[4].SetActive(false);
            obs_Chara[5].SetActive(false);
        }
    }

    /// <summary>
    /// 左からキャラクター表示
    /// </summary>
    /// <returns></returns>
    bool CharacterView()
    {
        if (anim_Map_UI.GetCurrentAnimatorStateInfo(0).IsName("Empty") == false)
        {
            CharaShowOrHideByJoined();
            anim_Map_UI.Play("Map_UI_Appear", 0);
            return false;
        }
        return true;
    }

    /// <summary>
    /// キャラクターを左に隠す
    /// </summary>
    void CharacterHide()
    {
        if (anim_Map_UI.GetCurrentAnimatorStateInfo(0).IsName("Map_UI_OutOfCanvas") == false && 
            anim_Map_UI.GetCurrentAnimatorStateInfo(0).IsName("Map_UI_Remove") == false &&
            anim_Map_UI.GetCurrentAnimatorStateInfo(0).IsName("Empty_Rem") == false)
            anim_Map_UI.Play("Map_UI_Remove", 0);
    }

    /// <summary>
    /// メニューを開く
    /// </summary>
    void MenuOpen()
    {
        CharacterView();
        CursorWaitCount = CursorWait;

        sound.DecisionSoundPlay();
        OpenTheMenu = true;
        // 動作禁止
        plycnt.isMoveAllow = false;
    }

    /// <summary>
    /// メニューを終了する。
    /// </summary>
    void MenuClose()
    {
        // 歩き許可
        plycnt.isMoveAllow = true;

        CharacterHide();
        TextAreaClose();
        CursorWaitCount = CursorWait;
        sound.CancelSoundPlay();
        OpenTheMenu = false;
        // 動作禁止
        plycnt.isMoveAllow = true;
    }
    /// <summary>
    /// スキルボタン
    /// TODO:まだまだ
    /// </summary>
    void SkillButtonEventFromUI(int n)
    {
        BeforeExternalProcessing(sklbook);
        sklbook.SetSkill(cd.PlayerData[n]);
        sklbook.ShowBook();
        return;
    }

    /// <summary>
    /// アイテムボタン
    /// TODO:まだまだ
    /// </summary>
    void ItemButtonEventFromUI(int n)
    {
        BeforeExternalProcessing(itembook);
        itembook.SetItem(cd.PlayerData[n]);
        itembook.ShowBook();
        return;
    }

    /// <summary>
    /// 外部処理前処理
    /// </summary>
    void BeforeExternalProcessing(object o)
    {
        // TODO:複数の処理をできるようにする。
        Book b = (Book)o;
        b.SetCallBack(this.EndExternalProcessing);

        isMenuActive = false;
    }

    /// <summary>
    /// 外部処理終了処理
    /// </summary>
    void EndExternalProcessing()
    {
        isMenuActive = true;
        return;
    }
}
