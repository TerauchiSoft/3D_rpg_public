﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Map_Audio : MonoBehaviour
{
    AudioSource audioSource;
    public AudioClip[] audios;

    // Use this for initialization
    void OnEnable () {
        audioSource = GetComponent<AudioSource>();
        AudioPlay(0);
    }
	
    public void AudioPlay(int audio_select)
    {
        audioSource.clip = audios[audio_select];
        audioSource.Play();
    }

	// Update is called once per frame
	void Update () {
		
	}
}
