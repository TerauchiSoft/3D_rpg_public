﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Map_Sound : MonoBehaviour
{
    AudioSource audioSource;
    public AudioClip[] sounds;
    /*
     * 0 ~ 4: 連続した足音
     * 5: 壁にぶつかった音
     * 6: 決定キー, メニュー開き
     * 7: キャンセルキー
     * 8: カーソル移動
     */
    readonly int Conc = 5;

    // Use this for initialization
    void OnEnable()
    {
        audioSource = GetComponent<AudioSource>();
    }

    int WalkFoot = 0;
    public void WalkSoundPlay()
    {
        SoundPlay(WalkFoot);
        WalkFoot = (WalkFoot < 4) ? WalkFoot + 1 : 0;
    }

    public void DecisionSoundPlay()
    {
        SoundPlay(6);
    }

    public void CancelSoundPlay()
    {
        SoundPlay(7);
    }

    public void CursorMoveSoundPlay()
    {
        SoundPlay(8);
    }

    public void WallHitPlay()
    {
        SoundPlay(Conc);
    }

    public void SoundPlay(int sounds_select)
    {
        audioSource.clip = sounds[sounds_select];
        audioSource.Play();
    }

    // Update is called once per frame
    void Update()
    {

    }
}
