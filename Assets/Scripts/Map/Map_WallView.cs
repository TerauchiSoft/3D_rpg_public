﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Map_WallView : MonoBehaviour {
    CommonData cd;
    public MapTest map;
    public Image[] img_map;
    public Image[] img_ws;
    public Image[] img_hs;
    public Image img_Pos;

    // Use this for initialization
    void Start ()
    {
        cd = GameObject.Find("cdObject").GetComponent<CommonData>();
        SetStepFloor(cd.intPos);
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        if (cd.isEndRead == false) return;
        if (cd.intPos != cd.BeforeFlamePos) SetStepFloor(cd.intPos);
    }

    /// <summary>
    /// 踏破状態を記録
    /// </summary>
    void SetStepFloor(CommonData.Position pos)
    {
        int n = (int)pos.x + pos.y * (int)map.Map_Size.x;
        StartCoroutine(ChangeColorToOne(img_map[n], 20, img_Pos));

        int x = pos.x; int y = pos.y;
        map.isStepfloors[x + (int)map.Map_Size.x * y] = 1;
        ViewStepWallAndFloor(pos);
    }

    /// <summary>
    /// 踏破した床、壁の表示有効化
    /// </summary>
    void ViewStepWallAndFloor(CommonData.Position po)
    {
        byte[] walls = new byte[4];

        for (int dir = 0; dir < walls.Length; dir++)
        {
            var position = new CommonData.Position(po.x, po.y, dir, po.floor);
            walls[dir] = (byte)map.WallInFront(position);
        }
        if (walls[0] > 0) StartCoroutine(ChangeColorToOne(img_ws[po.x + po.y * (int)map.Map_Size.x], walls[0]));
        if (walls[1] > 0) StartCoroutine(ChangeColorToOne(img_hs[po.x + 1 + po.y * (int)(map.Map_Size.x + 1)], walls[1]));
        if (walls[2] > 0) StartCoroutine(ChangeColorToOne(img_ws[po.x + (po.y + 1) * (int)map.Map_Size.x], walls[2]));
        if (walls[3] > 0) StartCoroutine(ChangeColorToOne(img_hs[po.x + po.y * (int)(map.Map_Size.x + 1)], walls[3]));
    }

    IEnumerator ChangeColorToOne(Image img, byte dat, Image Pos = null)
    {
        if (Pos != null) Pos.transform.position = img.transform.position;

        var col = img.color;
        if (dat == 2) col = new Color(1.0f, 0, 1.0f, img.color.a);
        float alphamax = 1.0f;
        if (dat == 20) alphamax = 0.35f;

        while(col.a < alphamax)
        {
            col.a += 0.1f;
            if (col.a >= alphamax)
            {
                col.a = 1.0f;
                img.color = col;
                yield break;
            }
            img.color = col;
            if (Pos != null) Pos.color = col;
            yield return 0;
        }
    }
}
