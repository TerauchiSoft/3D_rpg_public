﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using Utage;

// 位置イベントに対応
public class MapEventSystem : MonoBehaviour
{
    /*~----------------------------------/
    :                                    :
    :           外部のクラス             :
    :                                    :
    /~----------------------------------*/

    // 宴エンジン
    AdvEngine Engine { get { return utageEngine ?? (utageEngine = FindObjectOfType<AdvEngine>()); } }
    public AdvEngine utageEngine;

    // 共通データ
    public CommonData cd;

    // マップデータ
    public MapTest map;

    // フェーズマネージャー
    public PheseManager pm;

    // コントローラー
    PlayerController plycnt;

    /*~----------------------------------/
    :                                    :
    :    内部で使用するメソッドと変数    :
    :                                    :
    /~----------------------------------*/
    // イベントマップデータ
    List<Byte> iv_Map;

    // キャラ分岐用の乱数取得
    System.Random rd = new System.Random();
    int val_rand;
    

    // Use this for initialization
    void Start ()
    {
        // マップ
        map = gameObject.GetComponent<MapTest>();

        // 共通データ
        cd = GameObject.Find("cdObject").GetComponent<CommonData>();

        // 前行動時の位置初期化
        cd.BeforePosition = new CommonData.Position(-1, -1, 0, 1);

        // 宴
        utageEngine = GameObject.Find("AdvEngine").GetComponent<AdvEngine>();

        // コントローラー
        plycnt = GameObject.Find("cdObject").GetComponent<PlayerController>();
        plycnt.isMoveAllow = true;
    }

    /*~----------------------------------/
    :                                    :
    :        メインのメソッド            :
    :                                    :
    /~----------------------------------*/
    // 乱数更新
    private void Update()
    {
        val_rand = rd.Next() % 100;
    }
    
    // バトルシーンに移行
    public IEnumerator GotoBattleScene()
    {
        // 動作禁止
        plycnt.isMoveAllow = false;

        // メニュー禁止
        plycnt.isMenuAllow = false;

        // alphaのみ
        var col = new Color(0f, 0f, 0f, 0f);
        var ob = GameObject.Find("BlackMask").GetComponent<SpriteRenderer>();
        for (int i = 0; i < 20; i++)
        {
            ob.color = col;
            col.a += (float)1 / 20;
            yield return 0;
        }

        // フェーズ遷移
        pm.ChangeToGameMode("Battle", () =>
        {
            /*動作許可*/
            plycnt.isMoveAllow = true;
            ob.color = new Color(0f, 0f, 0f, 0f);
        });
    }

    /// <summary>
    /// バトルキャラ取得
    /// </summary>
    /// <returns></returns>
    string GetEnemyGroup(CommonData.Position po)
    {
        // まだまだ途中
        switch(po.floor)
        {
            case 1:
                var i = val_rand % 3;
                if (i == 0) { return "Wolf"; }
                if (i == 1) { return "FlowerSlime"; }
                if (i == 2) { return "Dragonbaby"; }
                return "";
            /*
            if (i == 0) { cd.PlayerDataSet(3); return "Wolf"; }
            if (i == 1) { cd.PlayerDataSet(1); return "FlowerSlime"; }
            if (i == 2) { cd.PlayerDataSet(20); return "Harpy"; }
            if (i == 3) { cd.PlayerDataSet(30); return "Yashigani"; }
            if (i == 4) { cd.PlayerDataSet(30); return "Alraune"; }
            return "";
            */
            default:
                return "";
        }
    }

    /// <summary>
    /// イベントチェック、CameraMoveから呼び出し
    /// </summary>
    public void IventCheck()
    {
        // iv_Map == null でロード
        if (iv_Map == null)
            iv_Map = GameObject.Find("マップ管理").GetComponent<MapTest>().GetIventMapData();

        // 前回と同一座標ならreturn
        if (cd.intPos.x == cd.BeforeFlamePos.x &&
            cd.intPos.y == cd.BeforeFlamePos.y)
        {
            return;
        }

        var np = cd.intPos;
        // ( x + z * (indexByte_0) + indexByte )の値を見る。
        var IventNo = iv_Map[np.x + np.y * (int)map.Map_Size.x];
        print("イベントNo : " + IventNo);

        // イベント呼び出し
        if (IventNo != 0)
        {
            IventRun(IventNo);
            return;
        }

        BattleEncounter();
        return;
    }

    /// <summary>
    /// バトルのエンカウント、バトルシーンに移行判定をする。
    /// </summary>
    public void BattleEncounter()
    {
        // 前回と同一座標ならreturn
        if (cd.intPos.x == cd.BeforeFlamePos.x &&
            cd.intPos.y == cd.BeforeFlamePos.y)
        {
            return;
        }


        // バトルシーンに移行
        var rand = new System.Random();
        if (rand.Next(30) == 0)//(rand.Next() % 20 == 0)
        {
            // データに現在座標をSave
            cd.RetPosition = cd.intPos;
            // print(flag.NowPosition.x + ", " + flag.NowPosition.y + ", " + flag.NowPosition.direction);


            // 敵キャラリストから敵グループ(prefab_Name)を読み込み
            cd.BattleEnemyGroupName = GetEnemyGroup(cd.intPos);

            // バトルシーンに移行開始
            StartCoroutine(GotoBattleScene());
        }
    }
    

    // イベント実行
    public void IventRun(int iv_No)
    {
        switch(map.Map_Floor)
        {
            case 1:
                Floor1(iv_No);
                break;
        }
    }

    // フロアごとのイベント
    void Floor1(int iv_No)
    {
        switch (iv_No)
        {
            // (4, 9)
            case 1:
                // スタートメッセージ再生したか
                if (cd.isStartMesAfter == true)
                    return;

                print("イベント1実行");
                // フラグ:スタートメッセージ再生後
                cd.isStartMesAfter = true;

                StartCoroutine(CoTalk("*はじまり"));

                break;
            // (4, 8)
            case 2:
                print("イベント2実行");
                StartCoroutine(CoTalk("*調べて冒険の助けに"));
                break;
            // (8, 9)
            case 3:
                print("イベント3実行");
                StartCoroutine(CoTalk("*エリナイベント"));
                break;
            // (4, 9)
            case 4:
                print("イベント4実行");
                StartCoroutine(CoTalk("*スイッチを調べよ"));
                break;
            // (1, 7)
            case 6:
                print("イベント6実行");
                StartCoroutine(CoTalk("*ヒビの入った壁初回"));
                break;
            case 7:
                print("イベント6実行");
                StartCoroutine(CoTalk("*宝箱1+メッセージ"));
                break;
            case 8:
                print("イベント6実行");
                StartCoroutine(CoTalk("*道に迷うよ"));
                break;
        }
        return;
    }




    IEnumerator CoTalk(string scenarioLabel)
    {
        // 動作禁止
        plycnt.isMoveAllow = false;

        // メニュー禁止
        plycnt.isMenuAllow = false;

        //「宴」のシナリオを呼び出す
        utageEngine.JumpScenario(scenarioLabel);

        //「宴」のシナリオ終了待ち
        while (!utageEngine.IsEndScenario)
        {
            yield return 0;
        }

        // 動作許可
        plycnt.isMoveAllow = true;
        // メニュー許可
        plycnt.isMenuAllow = true;
    }
}
